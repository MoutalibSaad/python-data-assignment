import streamlit as st

from multipage import MultiPage
from src.pages import eda, training, inference

app = MultiPage()

st.title("Card Fraud Detection Dashboard")
st.sidebar.title("Data Themes")

app.add_page("EDA", eda.app)
app.add_page("Training", training.app)
app.add_page("Inference", inference.app)

app.run()