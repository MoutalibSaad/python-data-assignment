from flask import Flask, request, jsonify

from flask_sqlalchemy import SQLAlchemy
from src.api.index_routes import blueprint as index_blueprint
from src.api.inference_routes import blueprint as inference_blueprint

from flask_marshmallow import Marshmallow
import os

app = Flask(__name__)

db = SQLAlchemy(app)
ma = Marshmallow(app)

basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'predictions.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.register_blueprint(index_blueprint)
app.register_blueprint(inference_blueprint)

from src.api.models import Prediction


class PredictionSchema(ma.Schema):
    class Meta:
        fields = ('feature11', 'feature13', 'feature15', 'amount', 'result')


prediction_schema = PredictionSchema()
predictions_schema = PredictionSchema(many=True)

db.create_all()


@app.route('/api/prediction/add', methods=['POST'])
def add_prediction():
    if request.method == 'POST':
        feature11 = request.json['feature11']
        feature13 = request.json['feature13']
        feature15 = request.json['feature15']
        amount = request.json['amount']
        result = request.json['result']
        return add_inference(feature11, feature13, feature15, amount, result)


@app.route('/api/predictions', methods=['GET'])
def get_predictions():
    if request.method == 'GET':
        all_products = Prediction.query.all()
        return predictions_schema.jsonify(all_products)


def add_inference(feature11, feature13, feature15, amount, result):
    pred = Prediction(feature11, feature13, feature15, amount, result)
    db.session.add(pred)
    db.session.commit()
    return prediction_schema.jsonify(pred)


if __name__ == '__main__':
    app.run()
