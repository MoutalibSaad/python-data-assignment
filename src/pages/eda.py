import streamlit as st

import pandas as pd
import matplotlib.pyplot as plt

from src.constants import DATASET_PATH


def app():
    st.header("Exploratory Data Analysis")

    @st.cache
    def load_csv():
        data = pd.read_csv(DATASET_PATH)
        return data

    df = load_csv()

    st.subheader("Statistical description")
    st.dataframe(df.describe())

    st.subheader("Dataset")
    choice = st.radio(
        "What do you wanna show?",
        ('Head', 'Tail', 'Full Dataset'))
    if choice == 'Head':
        st.write(df.head(100))
    if choice == 'Tail':
        st.write(df.tail(100))
    if choice == 'Full Dataset':
        st.write(df)

    st.subheader("Percentages of Valid and Fraudulent transactions")
    fraud = df[df.Class == 1]
    valid = df[df.Class == 0]
    fraud_percentage = (df.Class.value_counts()[1] / df.Class.value_counts()[0]) * 100
    info, chart = st.columns(2)
    with info:
        st.write('Fraudulent transactions percentage : %.3f%%' % fraud_percentage)
        st.write('Fraudulant Cases: ', len(fraud))
        st.write('Valid Cases: ', len(valid))
    with chart:
        fig1, ax1 = plt.subplots()
        ax1.pie([len(fraud), len(valid)], labels=['Fraudulant', 'Valid'], autopct='%1.3f%%', shadow=True)
        st.pyplot(fig1)

    st.subheader("Scatter Plot (Red = Fraudulant, Green = Valid)")
    xaxis, yaxis = st.columns(2)
    with xaxis:
        xAxis = st.selectbox('X Axis', list(df), index=2)
    with yaxis:
        yAxis = st.selectbox('Y Axis', list(df), index=1)
    df_sample = df.sample(frac=0.1, random_state=48)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    colors = ['green' if i == 0 else 'red' for i in df_sample["Class"]]
    ax.scatter(df_sample[xAxis], df_sample[yAxis], c=colors, alpha=0.8)
    ax.set_xlabel(xAxis)
    ax.set_ylabel(yAxis)
    st.pyplot(fig)
